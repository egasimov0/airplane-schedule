package org.qasimovey;

import lombok.RequiredArgsConstructor;
import org.qasimovey.domain.Plane;
import org.qasimovey.service.FlightService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.time.*;
import java.util.Arrays;
import java.util.List;

@Component
@RequiredArgsConstructor
public class MainApplication {
	private final FlightService flightService;

	public static void main(String[] args) {
		//scans the components defined under the package named "org.qasimovey"
		ApplicationContext context=new AnnotationConfigApplicationContext("org.qasimovey");
		MainApplication app = context.getBean(MainApplication.class);

		app.setUpDB();
		app.flightService.getAllFlights().forEach(System.out::println);
	}


	private void setUpDB(){
		List<Plane> planeList = Arrays.asList(
			Plane.builder()
					.belongTo("Istanbul")
					.duration(Duration.ofHours(3))
					.stayedTime(Period.ofDays(1))
					.name("Istanbul-Plane")
					.build(),
			Plane.builder()
					.belongTo("Paris")
					.duration(Duration.ofHours(5))
					.stayedTime(Period.ofDays(2))
					.name("Paris-Plane")
					.build(),
			Plane.builder()
					.belongTo("Beijing")
					.duration(Duration.ofHours(13))
					.stayedTime(Period.ofDays(2))
					.name("Beijing-Plane")
					.build(),
			Plane.builder()
					.belongTo("Tokyo")
					.duration(Duration.ofHours(15))
					.stayedTime(Period.ofDays(3))
					.name("Tokyo-Plane")
					.build(),
			Plane.builder()
					.belongTo("Moscow")
					.duration(Duration.ofHours(2))
					.stayedTime(Period.ofDays(1))
					.name("Moscow-Plane")
					.build(),
			Plane.builder()
					.belongTo("Madrid")
					.duration(Duration.ofHours(7))
					.stayedTime(Period.ofDays(2))
					.name("Madrid-Plane")
					.build());
		flightService.prepareFlightSchedule(/*LocalDateTime.of(LocalDate.of(2020,Month.JANUARY,1),LocalTime.of(0,0)),*/planeList);

	}

}
