package org.qasimovey.service;

import lombok.RequiredArgsConstructor;
import org.qasimovey.domain.Flight;
import org.qasimovey.domain.LandPlace;
import org.qasimovey.domain.Plane;
import org.qasimovey.repo.FlightRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
public class FlightService {
	private final FlightRepository flightRepository;

	public FlightService(FlightRepository flightRepository){
		this.flightRepository=flightRepository;
	}

	public void prepareFlightSchedule(/*LocalDateTime dateTime, */List<Plane> planes){
//		Map<LandPlace, Optional<Flight>> map = flightRepository.getFlights()
//				.stream()
//				.collect(Collectors.groupingBy(Flight::getLandPlace,
//						Collectors.maxBy(Comparator.comparing(Flight::getArrivalDate))));

		planes.stream()
				.forEach(this::addFlightSchedule);

		planes.stream()
				.forEach(this::addReturnFlightSchedule);
	}

	private void addFlightSchedule( Plane plane){
		LandPlace landArea = LandPlace.getNextAvailableLand();
		//update schedule info for next flight in particual area
		landArea.setEnrolledAt(landArea.getEnrolledAt().plus(landArea.getDuration()));
		this.flightRepository.save(
		Flight.builder()
				.id(Flight.getUniqueID())
				.landPlace(landArea)
				.arrivalDate(landArea.getEnrolledAt())
				.departureDate(landArea.getEnrolledAt().minus(plane.getDuration()))
				.from(plane.getBelongTo())
				.to("BAKU-AZ")
				.plane(plane)
				.isReturned(Boolean.FALSE)
				.build());
	}

	private void addReturnFlightSchedule(Plane plane){
		/*Flight lastFlight = flightRepository.getFlights()
				.stream()
				.filter((flight)->flight.getPlane().equals(plane))
				.sorted(Comparator.comparing(Flight::getArrivalDate).reversed())
				.findFirst()
				.orElseThrow(()->new RuntimeException("Plane was not in BAKU"));
		*/
		LandPlace availableLand=LandPlace.getNextAvailableLand();
		//mark the land as busy for some duration
		availableLand.setEnrolledAt(availableLand.getEnrolledAt().plus(availableLand.getDuration()));

		flightRepository.save(Flight.builder()
				.from("BAKU-AZ")
				.to(plane.getBelongTo())
				.departureDate(availableLand.getEnrolledAt())
				.arrivalDate(availableLand.getEnrolledAt().plus(plane.getDuration()))
				.id(Flight.getUniqueID())
				.plane(plane)
				.landPlace(availableLand)
				.isReturned(Boolean.TRUE) //is it return flight ?
				.build());

	}
	//it needs to enhanced
	private void addFlightScheduleBAD(Plane plane){
		if (!flightRepository.getFlights().isEmpty()) {
			Map<LandPlace, Optional<Flight>> map = flightRepository.getFlights()
					.stream()
					.collect(Collectors.groupingBy(Flight::getLandPlace,
							Collectors.maxBy(Comparator.comparing(Flight::getArrivalDate))));

			Optional<Flight> planeInfo = map.values()
					.stream()
					.map(optionalFlight -> optionalFlight.get())
					.min(Comparator.comparing(Flight::getArrivalDate));


			flightRepository.getFlights().add(Flight.builder()
					.from(plane.getBelongTo())
					.to("BAKU")
					.departureDate(planeInfo.get().getArrivalDate().plus(planeInfo.get().getLandPlace().getDuration()))
					.arrivalDate(planeInfo.get().getArrivalDate().plus(planeInfo.get().getLandPlace().getDuration()))
					.landPlace(/*planeInfo.get().getLandPlace()*/LandPlace.Land_2)
					.id(Flight.getUniqueID()).build());
			return;
		}

		flightRepository.getFlights().add(Flight.builder()
				.from(plane.getBelongTo())
				.to("BAKU")
				.departureDate(LocalDateTime.now())
				.arrivalDate(LocalDateTime.now().plus(plane.getDuration()))
				.landPlace(LandPlace.Land_1)
				.id(Flight.getUniqueID())
				.build());

	}

	public List<Flight> getAllFlights(){
		return flightRepository.getFlights();
	}
}
