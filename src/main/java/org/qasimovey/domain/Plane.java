package org.qasimovey.domain;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.time.Duration;
import java.time.Period;

@Data
@Builder
@ToString(exclude = {"stayedTime","belongTo"})
public class Plane {
	private String name;
	private Duration duration;
	private Period stayedTime;
	private String belongTo;
}
