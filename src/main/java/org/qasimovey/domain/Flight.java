package org.qasimovey.domain;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@ToString(exclude = "isReturned")
@Builder
public class Flight{
	private static long counts = 0;
	private Long id;
	private String from;
	private String to;
	private LocalDateTime departureDate;
	private LocalDateTime arrivalDate;
	private LandPlace landPlace;
	private Plane plane;
	private Boolean isReturned;

	public static Long getUniqueID(){
		return counts++;
	}
}
