package org.qasimovey.domain;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAmount;
import java.util.Arrays;
import java.util.Comparator;

public enum LandPlace {
	Land_1("Haydar Aliyev","Heydar Aliyev International Airport - Azerbaijan Airlines",Duration.ofMinutes(15)),
	Land_2("GYD","GYD Baku International Airport",Duration.ofMinutes(15));

	private String description;
	private String name;
	private Duration duration;
	private LocalDateTime enrolledAt;

	LandPlace(String name,String description,Duration duration){
		this.description=description;
		this.name=name;
		this.duration=duration;
		this.enrolledAt=LocalDateTime.now();
	}

	public LocalDateTime getNextAvailableTime(){
		return this.enrolledAt.plus(this.getDuration());
	}

	public TemporalAmount getDuration() {
		return duration;
	}

	public String getDescription() {
		return description;
	}

	public LocalDateTime getEnrolledAt(){
		return this.enrolledAt;
	}
	public void setEnrolledAt(LocalDateTime enrolledAt){
		this.enrolledAt=enrolledAt;
	}

	public static LandPlace getNextAvailableLand(){
		return Arrays.stream(LandPlace.values())
				.sorted(Comparator.comparing(LandPlace::getEnrolledAt))
				.findFirst()
				.get();
	}
/*
	@Override
	public String toString() {
		return "LandPlace{" +
				"description='" + description + '\'' +
				'}';
	}
	*/

}
