package org.qasimovey.repo;

import org.qasimovey.domain.Flight;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class FlightRepository {
	private final List<Flight> flightList;

	public FlightRepository(){
		flightList = new ArrayList<>();
	}

	public List<Flight> getFlights(){
		//for now it is ok to return direct reference, but need to be avoided
		return this.flightList;
	}
	public void cleanUp(){
		flightList.clear();
	}
	public void save(Flight flight){
		this.flightList.add(flight);
	}
}
